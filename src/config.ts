import convict from 'convict';

const configObject = convict({
  projectID: {
    doc: '',
    format: Number,
    default: 0,
    env: 'PROJECT_ID',
  },
  token: {
    doc: '',
    format: String,
    default: '',
    env: 'TOKEN',
  },
});

export const config = configObject.getProperties();
