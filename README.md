# Delete merge requests on gitlab

This is the a script I used to delete all the merge requests belonging to Unreal Engine's source code. Probably there is a better way to do it.
I wrote about the background here [background](https://forum.gitlab.com/t/deleting-all-merge-requests-belonging-to-a-project/46178), but basically I tried resolving the following error:

```
This project was scheduled for deletion, but failed with the following message: PG::QueryCanceled: ERROR: canceling statement due to statement timeout CONTEXT: SQL statement "UPDATE ONLY "public"."merge_requests" SET "source_project_id" = NULL WHERE $1 OPERATOR(pg_catalog.=) "source_project_id""
```

How to use it:

```shell
PROJECT_ID=YOUR_PROJECT_ID
TOKEN=YOUR_ACCESS_TOKEN
yarn && yarn delete-mrs
```
